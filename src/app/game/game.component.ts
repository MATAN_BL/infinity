import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  @Input() gameName: string;

  constructor() { }

  ngOnInit() {

  }

}
