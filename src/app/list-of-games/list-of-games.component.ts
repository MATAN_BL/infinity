import { Component, OnInit } from '@angular/core';
import {Service} from "../service";

@Component({
  selector: 'list-of-games',
  templateUrl: 'list-of-games.component.html',
  styleUrls: ['list-of-games.component.scss']
})
export class ListOfGamesComponent implements OnInit {

  constructor(private service: Service) { }

  ngOnInit() {
  }

}
