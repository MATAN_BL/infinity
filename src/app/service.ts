import {Injectable, OnInit} from '@angular/core';

@Injectable()
export class Service implements OnInit{
  games: Object;
  mode: string = "featured"; // can be "featured", "slots", "card" and "table"
  listOfGames: Object;

  constructor() {
    this.games = require('./games.json');
    this.listOfGames = this.games[this.mode];
    console.log(this.games);
  }

  changeMode(newMode: string) {
    this.mode = newMode;
    this.listOfGames = this.games[newMode];
    console.log(`new mode is ${this.mode}`);
    console.log(this.games[newMode])
  }

  getUrlForImage(name: string) {
    let newName = name.toLowerCase();
    newName = newName.replace(/[^a-z]/g, '');
    let urlStart = "http://cacheimg.majesticslots.com/images/www/games/minipods";
    return `url(${urlStart}/${newName}-minipod.jpg)`;
  }

  clickGame(game: any) {
    this.openGame(game.game_name, game.game_code, game.machine_id, game.denominations, game.hands);
  }

  openGame(game_name, game_code, machine_id, denominations, hands) {
    alert(`game_name: ${game_name}, game_code: ${game_code}, machine_id: ${machine_id}, \
denominations: ${denominations}, hands: ${hands}`);
  }

  ngOnInit() {

  }


}
